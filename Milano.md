# Milano

## Office

Our Agile Lab office in Milan is located at [Spaces Farini](https://www.spacesworks.com/milan/farini/):

- **Address:** [Via Giuseppe Piazzi, 2-4, 20159 Milano (MI)](https://maps.app.goo.gl/42bnxHxmSjS2UrySA)
- **Seats:** There are 9 available workstations, divided as follows:
  + Office 223: 6 desks.
  + Office 224: 3 desks.

  Moreover, there is also the office 225, but it's currently unavailable.
- On days with high attendance, additional **open space** desks can be used, or **meeting rooms** can be booked (upon request to the Milan [PeopleCare/Facility](https://app.holaspirit.com/o/5d934a98a626a353151327d5/governance/roles/all#circle-5e98e5d9dad10f01911db3c4/member-5e98f64b8d5a694ba7104a52/circle-5e98e5d9dad10f01911db3c4/role-5e99064a094ee80565620bab)).
- **Facilities layout:**
  + On the **ground floor**, you will find the reception and the open space.
  + On the **second floor**, in addition to our office, there is an outdoor terrace.
  + On the **top floor**, there is a bar with a lounge area and a large panoramic outdoor terrace with tables.

### How to reach us
- **Public transportation**:
  + **Suburban trains:** S1, S2, S5, S6, S12, S13 – stop at *Milano Lancetti* (approx. 8 minutes on foot).
  + **Metro line M3 (yellow):** stop at *Maciachini* (3 stops from Central Station, approx. 8 minutes on foot).

  Public transportation in Milan is handled by [ATM](https://www.atm.it/en/Pages/default.aspx). It is possible to purchase ATM tickets for the metro downloading the [*ATM Milano Official App*](https://www.atm.it/en/ViaggiaConNoi/Pages/ATMMobile.aspx).
- **Bike** with a dedicated parking available.

### Entrance procedure
Access to the building is authorized only with a valid badge.
- **Access via a colleague:** You can enter by asking a Milan-based colleague with a permanent badge to open the door for you (recommended for short visits).
- **Temporary badge reservation:**  You can reserve your temporary badge.
  + The badge is personal and must be reserved by contacting the Spaces reception (Monday - Friday, 8:30 a.m. - 7:00 p.m.) through the Milan [PeopleCare/Facility](https://app.holaspirit.com/o/5d934a98a626a353151327d5/governance/roles/all#circle-5e98e5d9dad10f01911db3c4/member-5e98f64b8d5a694ba7104a52/circle-5e98e5d9dad10f01911db3c4/role-5e99064a094ee80565620bab) (or directly, copying both the Facility and the [PeopleCare/Lead Link](https://app.holaspirit.com/o/5d934a98a626a353151327d5/governance/roles/all#circle-5e98e5d9dad10f01911db3c4/member-5e98f64b8d5a694ba7104a52/circle-5e98e5d9dad10f01911db3c4/role-5e98e5d9dad10f01911db3d3)).
  + Specify the date(s) you plan to visit.
  + The temporary badge must be picked up in the morning at the reception and returned at the end of the day.  
  + The temporary badge issuance is free of charge.

### Desk booking
It is recommended to book your workstation by adding your name and the day of your presence in the **attendance document** available in the Milan Teams channel (for more information, contact the Milan [PeopleCare/Facility](https://app.holaspirit.com/o/5d934a98a626a353151327d5/governance/roles/all#circle-5e98e5d9dad10f01911db3c4/member-5e98f64b8d5a694ba7104a52/circle-5e98e5d9dad10f01911db3c4/role-5e99064a094ee80565620bab)).

### Time tracking
Please, remember to **log your time** spent in the Milan office in [Elapseit](https://app.elapseit.com/timesheet) as *OP - Milano Office Presence* to ensure accurate monitoring of space usage.

## Patronal feast
The office will be closed on **August 16th** and **December 7th** (Saint Ambrose, the patron saint of Milan).

## Emergency guidelines
You can find the emergency procedure guidelines at the following [link](https://agilelab.sharepoint.com/:f:/s/bigdata/EkqRcGZi7txOp8fpE1IhQ-8Bxs9mixtVTywNsNDVIwGCRw?e=jm16w8).