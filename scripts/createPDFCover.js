// Expects to receive MR number as 1st argument.
const jimp = require('jimp')

const outputFile = './cover.jpg'
const srcFile = './images/coverSrc.jpg'

var fontType = jimp.FONT_SANS_64_BLACK
var xPos = 0 
var yPos = 0
var printText = 'Update: '
var mrNumber = process.argv[2] || 'None';

let font = null

jimp.loadFont(fontType)
	.then((loadedFont) => {
		font = loadedFont
		return jimp.read(srcFile)
	})
	.then((src) => {
		src
			.print(
				font,
				xPos,
				yPos,
				{
					text: printText + mrNumber,
					alignmentX: jimp.HORIZONTAL_ALIGN_CENTER,
					alignmentY: jimp.VERTICAL_ALIGN_MIDDLE,
				},
				src.bitmap.width,
				src.bitmap.height,
			)
			.writeAsync(outputFile)
			.then(() => {
				console.log(`Cover created on: ${outputFile}`)
			})
	})
	.catch((err) => {
		console.log('Error', err);
	})