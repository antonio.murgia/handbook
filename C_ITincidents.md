Purpose

The purpose of this policy is to:

Describe the types of security incidents.

Provide details on how incidents can and will be handled.

Identify responsibilities for reporting and managing incidents.

Outline detailed procedures for incident notification and processing.

Agilelab is responsible for the security and integrity of all data in its possession. Agilelab must protect this data using all necessary means to ensure that any incident that could cause harm to assets and reputation is prevented and/or minimized at all times.

There are many types of incidents that could have implications for security and operational continuity:

A computer security incident is an event that negatively affects the processing or use of a computer. This includes:

Loss of information confidentiality.

Compromised information integrity.

Denial of service.

Unauthorized access to systems.

Improper use of systems or information.

Theft and damage to facilities.

Virus attacks.

Human intrusion.

Other incidents include:

Missing correspondence.

Exposure of uncollected prints.

Misplaced or missing media.

Unintentional transmission of passwords.

Ensuring effective reporting and management of security incidents will help reduce and, in many cases, prevent them.

Operational Procedures

Information Security Management Responsibilities

The incident management responsible parties are the RTD (Responsible for Technical Development) and RIT (Responsible for Information Technology).

Incident Reporting

Any violation of the incident management policy must be reported as soon as possible through this instruction.

Security incidents can be reported in various ways. It is recommended to record violations as follows:

Open a support ticket to manage the issue and notify the support department, which will plan a technical intervention.

Contact the IT Manager by phone.

There are other ways to record violations, including sending an email to the IT Manager.

Reporting Weak Points

All personnel using computer systems are required to report to the RTA (Responsible for Technical Applications) any observed, suspected, or identified weak points related to information security. The preferred reporting methodology in these cases is via email to the RTA, known to all employees in the data controller's area. It will then be the responsibility of the RTA to notify the RTD and RIT of such reports, using the company email addresses of the RTD and RIT as the preferred channel.

Evaluation and Event Decisions

Once a problem is detected using the alert and monitoring system, the procedure involves handing over the problem to second-level support, working closely with the RIT. The analysis of the problem is usually carried out by second-level support, which identifies the issues, isolates any malfunctions that could cause problems for a large number of clients if possible, and decides on a resolution strategy.

Incident Management

When an incident is reported, the information security officer will determine whether the incident should be resolved as soon as possible in collaboration with the most appropriate personnel for handling. Representatives investigating security breaches will be responsible for updating, modifying, and changing the status of incidents.

All parties involved in security incidents commit to:
Analyze and determine the cause of the incident and take all necessary measures to prevent its recurrence.

Report to all interested parties and maintain communication and confidentiality throughout the incident investigation.

Identify issues caused by the incident and further prevent or reduce their impact.

Contact third parties to resolve software errors/failures and maintain contact with the IT service to ensure compliance with contractual agreements and legal requirements and minimize potential disruptions to other Agilelab systems and services.

Ensure that all system records and logs are securely stored and available to authorized personnel when necessary.

Ensure that only authorized personnel have access to systems and data.

Ensure that all documentation and notes are accurately stored, recorded, and made available to authorized personnel.

Ensure the implementation and monitoring of the effectiveness of all authorized improvement measures.

All recorded incidents must include all details of the incident, including any actions/resolutions or connections to other known incidents. Initially resolved but recurring incidents will be reopened or a new report will be created referring to the previous one.

Monthly reports on generated incidents are sent to the IT Manager to facilitate monitoring of the types, numbers, frequency, and severity of incidents, which will contribute to correcting and preventing their recurrence.

During incident investigations, Agilelab's hardware, logs, and records may be analyzed by the Audit function. Information and data may be collected as evidence to support any disciplinary or legal actions. Confidentiality must always be maintained during such investigations.

The IT Manager is initially responsible for managing security incidents and will decide whether an incident should be "handed over" and handled (including closure) by the FA (Functional Applications) if necessary.

Appendix A: Incident Reporting Process Diagram

Agilelab has a clear incident reporting mechanism that describes in detail the procedures for identifying, reporting, and recording security incidents. By continuously updating and informing employees, partners, contractors, and suppliers about the importance of identifying, reporting, and taking necessary actions to address incidents, Agilelab can continue to be proactive in addressing incidents as they occur.

All Agilelab employees, partners, and suppliers are required to report all incidents, including potential or suspected incidents, as soon as possible through this incident reporting instruction.

The types of incidents covered by this policy include, but are not limited to:

Computers left unlocked when unattended.

Users of Agilelab computer systems are continuously reminded of the importance of locking their computers when not in use or when leaving them unattended for a period of time. All Agilelab employees and suppliers must ensure that they appropriately lock their computers, even though Agilelab systems are configured to automatically lock after 5 minutes of inactivity. The detection of an unlocked computer that is unattended must be reported through Agilelab's incident reporting procedures.

Password Information

Unique account IDs and passwords are used to enable individual access to systems and data. It is imperative that individual passwords are not disclosed to others, regardless of trust. If a person needs access to data or a system, they must go through the correct authorization procedures. If someone suspects that their password or another user's password has been intentionally, unintentionally, or accidentally disclosed, it must be reported through Agilelab's incident reporting procedures. Under no circumstances should an employee allow another employee to use their user account details after logging into a system, even under supervision.

Antivirus Warnings/Alerts

All desktop computers, laptops, and tablets used in Agilelab are equipped with antivirus software (including anti-spyware/malware). For the most part, the interaction between the computer and the antivirus software will go unnoticed by computer users. Occasionally, an antivirus warning message may appear on the computer screen. The message may indicate the presence of a virus that could cause loss, theft, or damage to Agilelab's data. The warning message may indicate that the antivirus software may not be able to resolve the issue, and therefore it must be reported to the IT Manager as soon as possible.

Loss of Media

The use of portable media such as CDs/DVDs, USB drives/disk drives for data storage requires the user to be fully aware of the responsibilities associated with the use of such devices. The use of PCs, laptops, tablets, and many other portable devices increases the possibility of data exposure and vulnerability to unauthorized access. Any authorized user of a portable device who encounters misplaced or suspected damaged, intentionally or accidentally stolen portable media, must report it immediately through Agilelab's incident reporting procedures.

Data Loss/Disclosure

The potential for data loss applies not only to portable media but also to all data that is:

Transmitted over a network and intended for an unintended and unauthorized recipient (e.g., sending sensitive data via email).

Intercepted on the internet through insecure channels.

Inputted on the internet, accidentally or intentionally.

Posted on Agilelab's website and identified as inaccurate or inappropriate (which should be reported).

Conversationally disclosed during conversation.

Printed or media-based unauthorized disclosure by employees or misguided representative to the press or media.

Data that can no longer be located and is no longer accounted for in a computer system.

Paper copies of data and information that are no longer locatable.

Paper copies of information and data accessible from desks and unattended areas.

All Agilelab employees, partner agencies, and suppliers must act responsibly and professionally and always be aware of the importance of maintaining the security and integrity of Agilelab's data.

Any loss or disclosure of data, intentional or accidental, must be reported immediately using Agilelab's incident reporting procedures.

Abuse of Personal Information

All information that enables the identification of an individual, such as home address, banking details, etc., must not be disclosed, discussed, or transmitted to any person who is not authorized to view, disclose, or distribute it. Any abuse/improper use of such identifiable information must be reported through Agilelab's incident reporting procedures.

Physical Security

It is essential to maintain the physical security of offices and rooms where data is stored, maintained, displayed, or accessed. Rooms or offices specifically designated as areas where secure information is located or stored must have a method to physically secure access to such areas, such as a combination lock mechanism. Ground-level windows may also provide access to the room/office and must be securely locked, especially when the room is unattended. Rooms that have not been protected must not be used to store sensitive and personal information; concerns regarding rooms/offices that need to be securely closed or restricted access must be reported to the IT Manager through Agilelab's incident reporting procedures.

Logical Security/Access Controls

Controlling, managing, and limiting access to Agilelab's network, databases, and applications is an essential part of information security. It is necessary to ensure that only authorized employees can access electronically processed and stored information.

Missing Correspondence

Data or information sent electronically or physically that cannot be accounted for, such as not reaching the intended destination via mail, not being sent electronically, sent for printing but not printed, etc., must be reported through Agilelab's incident reporting procedures.

Found Correspondence/Media

Data stored on any storage media or physically printed information that has been found in a location other than a secure location where the security and integrity of the data/information may be compromised by unauthorized viewing and/or access (e.g., unlocked prints) must be reported through Agilelab's incident reporting procedures.

Loss or Theft of Information/Computer Data

Data or information that can no longer be located, such as not found in an expected location, archives, etc., or known or suspected to have been stolen, must be reported immediately through Agilelab's incident reporting procedures.

Incident Response

Once the resolution strategy has been decided, there are three possible scenarios:

The strategy requires an on-site physical intervention. In this case, second-level support arranges an intervention.

The resolution strategy allows for remote problem resolution. In this case, second-level support implements all necessary measures to resolve the problem by remotely connecting to the equipment.

The resolution strategy involves involving the RIT. An example could be a broken disk inside one of the machines in the data center. In this case, the RIT promptly repairs the failure.

Learning from Incidents

Once the incident management procedure is completed, which coincides with the closure of the associated support ticket, an analysis is conducted to understand if the problem is "standard," meaning it falls within the cases where support can already provide a comprehensive and timely response, or if the problem arises from new cases not covered by the problem resolution procedures.

In the case of a "new" problem or one not covered by procedures, an analysis of the problem is carried out, and in agreement with second-level technical support, the RTD and RIT establish a procedure capable of resolving or at least limiting the problems arising from that particular case. The preferred method for deciding on a new procedure for unknown issues is through a meeting involving all the aforementioned individuals.

Evidence Collection

The RIT records all necessary information on the ticket, clearly explaining the encountered problem, problem analysis, how it was resolved, any final tests performed, where the problem may affect other supports, and the RIT performs targeted checks to verify the correct functioning of the equipment in advance. During the Review, the reported/encountered tickets during the year are analyzed.