# Padova

## How to reach us

- Address: [Bassilab - Via Ugo Bassi, 60, 35131 Padova PD](https://maps.app.goo.gl/ZRRGr5RoXcg7x9mN7)
- [Website](https://www.bassilab.it/)
- 15' walk from the station
- Parking:
  + 20 free outdoor parking spots available inside the structure (open to everyone in the coworking space). You need to be authorized in [AirKey](https://play.google.com/store/apps/details?id=com.evva.airkey) to open the gate (see [Office](Padova.md/Office)).
  + 6 bike parking spots (mostly uncovered)
- Public transportation in Padua is handled by [Busitalia Veneto](https://www.fsbusitalia.it/content/fsbusitalia/it/veneto/orari-e-linee.html):
    + By bus (from train station):
        * Line U10 from _Stazione Fs_ to _Via Tommaseo stop_ + 4' walk
        * Line U25 from _Stazione Fs_ to _Via Tommaseo stop_ + 4' walk
        * Just walk if it doesn't rain
    + Other than ordinary means of purchase, it is possible to purchase tickets via Trenitalia App in `Purchase > Services > Busitalia Buses > Padova` and choosing the required ticket.

## Office
In order to access the coworking area (included parking) you need the authorization in [AirKey](https://play.google.com/store/apps/details?id=com.evva.airkey) application. Please refer to the facility manager (People Care > Facility in [Hola](https://app.holaspirit.com/)) to ask for authorization.

The office is located in the mezzanine floor, in front of you as you enter the main wooden door.

### Office spaces
 - _Operative area_: private office with 5 desks, equipped with 4 external screens, several keyboards and mouses. There's a coffee machine and a microwave.

 - 1 common conference room you can book in advance via mail ([info@bassilab.it](mailto:info@bassilab.it)) or just use it when needed

### Office hours
- Monday to Friday: 8AM - 8PM
- Saturday and Sunday: closed

> [!Danger] Employees must respect the times above: at 8PM the alarm is automatically switched on, and the parking gate cannot be open after closing hour.

## Patronal Feast
- The office will be closed on **June 13th**.

## Emergency guidelines
You can find the emergency procedure guidelines at the following [link](https://agilelab.sharepoint.com/:f:/s/bigdata/EmSu2KefpAtLrn7-bP-zflYBP_kUX3gxu2_L2DldrUj_Gg?e=49UzbP)