# Certification Guidelines
At Agile Lab, we believe in continuous professional growth as the key to technical excellence and individual success. Certifications provide an opportunity to acquire new skills, demonstrate industry expertise, and strengthen the company's market positioning. Investing in personal growth ensures quality for all our stakeholders.  

At the beginning of each year, the {% role %}Agile Lab/CTO{% endrole %} defines the company's technical strategy, identifying key technologies and the skills to develop. 

The {% role %}Consulting/Certification Coach{% endrole %} creates a new **Certification Plan** by cloning the previous year's document and updating it based on the technical strategy defined by the CTO, which establishes:  
- Which certifications are a priority for the current year; 
- Which certifications need to be maintained and renewed. 

This plan is dynamic and can be updated throughout the year to adapt to new business needs or emerging opportunities.  

You can find the updated plan in the SharePoint under the path `BigData/Certifications/Certification Plan 202x`. 

## Certification domains 
All certifications must be related to topics relevant to the roles and requirements outlined in the company's ladder. Here are some example areas:  
- Data Science;  
- Data Engineering; 
- Data Analysis; 
- Software Engineering; 
- Leadership and Management; 
- Etc. 

If you are unsure whether the certification you wish to pursue aligns with the certification domains, you can consult the Certification Coach for further clarification. We are happy to guide you. 

## Certification classification 
Certifications are divided into two categories:  
- **Priority 1 (P1)**: Certifications included in the Certification Plan. These are considered strategic for the company and receive the highest priority in terms of support and incentives.  
- **Priority 2 (P2)**: Certifications not included in the plan or obtained in a higher number than planned. While still supported, they are not considered strategic for the company and therefore have a different level of priority.  

Certification renewals follow the same logic. If the renewal requires a new paid exam, the certification is treated as new and follows the priority rules. 

## Certification process 
### 1. Planning and choosing the certification 
The first step in obtaining a certification is planning. If you already have a clear idea of the certification you want to achieve, or if you need guidance on which one best fits your career path, the starting point is a **one-to-one meeting with the Certification Coach**.  

During this meeting, you can discuss:  
- Your professional goals and the skills you want to develop;  
- The technologies you are working with and their relevance to Agile Lab's strategy;  
- The most suitable path for you, considering the available certification domains. 

If the desired certification is included in the Certification Plan, you will need to add a row with your full name, setting the status to `Intention` and entering an estimated exam date in the appropriate fields of the Certification Plan.  

If the certification is not in the plan, it will be evaluated by the Certification Coach:  
- If a similar certification is already included in the Certification Plan, priority will be given to that one (you can still pursue the original certification at a later stage);  
- If the certification aligns with the available certification domains, it will be classified as P2 and supported accordingly. This phase is essential to define a clear path and ensure you have the right resources for preparation. 

### 2. Preparation and time management 
Exam preparation is a crucial part of the process. Agile Lab offers support in different ways:  
- If the certification is P1, you are entitled to up to 12 additional working hours (depending on the certification's difficulty) from your time training budget to dedicate to preparation and taking the exam. Refer to the Training section of [Welfare & Benefits](WelfareAndBenefits.md) for details regarding the standard training budget, and check the Certification Plan to see how many extra hours you can get for each certification.  
- If the certification is classified as P2, you can still use your standard training budget to purchase courses, books, or attend events (both online and offline). However, no additional working hours are provided.  

To ensure a balance between study and work activities, your Team Leader will help you:  
- Organize your time effectively;  
- Monitor progress and address any challenges;  
- Integrate study with relevant work projects and activities.  

In addition to the support from the Team Leader and Certification Coach, you will have access to:  
- The [Training Portal](TrainingPortal.md), which contains courses, guides, study materials, and tips relevant to your certification;  
- The SharePoint folder `BigData/Certifications`, which includes additional study materials shared by colleagues who have already obtained a certification;  
- The Teams channel `BigData/Certifications`, where you can ask for advice and suggestions. 

### 3. Booking the certification exam 
Once you have completed your study plan, you will need to proceed with booking your exam. A couple of months before your estimated exam date, contact the Certification Coach to check if any vouchers or discounts are available for your certification exam.  

If no vouchers are available, you can proceed with self-purchasing the exam and request reimbursement via Elapseit, following the guidelines in [Spending Company Money](SpendingCompanyMoney.md).   

After booking the exam, update the Certification Plan by selecting:  
- `Booked P1` if the current number of P1 bookings plus the number of certifications obtained is still below the required amount; 
- `Booked P2` otherwise.  

If you wish to book an exam earlier than those already scheduled and gain priority 1, contact your Certification Coach to check if this is possible.  

You can also verify which bookings are available in the `Priority Check` field at the bottom of the document.  

### 4. Monitoring and follow-up 
The certification process is a significant commitment, so it will be monitored regularly through:  
- Periodic check-ins with your Team Leader to assess progress and overcome any obstacles;  
- Ongoing support from the Certification Coach, who will track your progress and help you optimize your preparation.  

The Certification Coach performs a weekly review of the Certification Plan to ensure that the company's certification roadmap is up to date and that all candidates have a clear study strategy.  

Specifically, the Coach:  
- Checks whether candidates have updated their status in the plan (e.g., from "Intention" to "Booked"); 
- Contacts those who have not communicated their study plan to ensure alignment with business objectives;  
- Identifies candidates for unassigned certifications to ensure that strategic skills are covered. 

### 5. Completing the exam and recording the certification 
After taking the exam, you must inform the Certification Coach of the result.  

If you pass the exam:  
- Update the Certification Plan by setting the status to `Achieved` send an update to the Certification Coach;  
- The Certification Coach will notify the management and update the official certification registry; 
- Your achievement will be announced in the Teams channel `Big Data/Certifications`, sharing the milestone with colleagues and promoting a culture of continuous learning;  
- If the certification was classified as P1, you will be eligible for a reward, which will be granted at the end of the year according to the defined rules.

If you fail the exam:  
- You can retake the exam, but you must still report the result to the Certification Coach;  
- For P1 certifications, if you pass on the second attempt, the **reward will be reduced by 50%**;  
- Further attempts must be agreed upon with the Certification Coach, as funding will not be available for a third attempt, except in exceptional cases.  

To foster knowledge sharing, always document your exam experience (tips and insights are welcome) in the company's Big Data OneNote, under the dedicated certifications section. If a section for your specific exam already exists, add your experience there. 

## The value and recognition of certification 
### Certification impact  
Obtaining a certification is an important step in your professional growth, and Agile Lab is with you at every stage of the journey. Follow the process, make the most of the available resources, and remember that each certification is an achievement that belongs to you forever and may have an impact on the company, so you are encouraged to include it in your pitch (see [Engineering Ladder](EngineeringLadder.md)).  

### Incentives and rewards 
The reward for achieving a certification is a tangible recognition of individual effort. However, its true value is not just financial—it lies in professional growth, acquired skills, and the prestige the certification brings to your career.  

## Support and management 
### Organizing study materials 
All study materials must be saved in SharePoint under `BigData/Certifications` and published on the Training Portal, following the standard naming convention to facilitate search and access.  

To improve and speed up the search for previously purchased or available training material, please use the full name of the certification (e.g., Microsoft Certified: Azure Fundamentals).  

If you know of existing materials stored in another SharePoint folder, you are strongly encouraged to move them to the specified folder or publish a dedicated page following the Training Portal policies.  

The following materials are allowed:  
- Official guides;  
- Shared notes; 
- Practice exercises;  
- Links to courses.  

### The role of the Certification Coach 
The Certification Coach is the main point of contact for everything related to certifications. Their responsibilities include:  
- Managing the onboarding process for new Agilers, verifying if they have relevant prior certifications for Agile Lab;  
- Monitoring the Teams channel `BigData/Certifications`, providing support and answering questions;  
- Creating and managing the Certification Plan, updating it regularly;  
- Organizing one-to-one meetings to help you choose the best certification path;  
- Tracking candidates' progress, offering support and study resources;  
- Ensuring the renewal of company certifications, making sure partnership requirements and internal skill levels are maintained;  
- Promoting and recognizing achievements, communicating results to management and across the company.  

If you have any questions or want to start your certification journey, contact the Certification Coach.  

### Certification renewal 
The Certification Coach will monitor expiring certifications and contact certificate holders to evaluate renewal options. 

If a certification remains strategic for the company and is part of the Certification Plan, its renewal will be treated as P1. If it is no longer relevant, you can choose to renew it personally as a P2, following the previously described rules.  

### Managing certifications when leaving the company  
If a certified employee leaves Agile Lab, the Certification Coach will:  
- Remove their name from the certification registry;  
- Check if the certification covers a strategic requirement and, if necessary, identify a replacement to maintain the company's skill level.  

## Frequently asked questions (FAQ) 
_What happens if I fail a P1 certification?_  
You can take a second attempt. In this case, the reward will be reduced by 50%, while the exam cost will still be deducted from your training budget. For a third attempt (or further), no rewards or reimbursements will be provided for the certification cost.  

_Who supports me in my certification journey?_  
You will plan your study path with the Certification Coach and implement it with the support of your Team Leader, who will be your daily point of reference.  

_Can I obtain certifications not included in the Certification Plan?_  
Absolutely! They will be classified as P2, as long as they align with the certification domains. Schedule a meeting with the Certification Coach for more details.  
